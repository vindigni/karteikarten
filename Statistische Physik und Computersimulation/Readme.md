== VORGEHEN ==
1. Schreibe die Fragen und Antworten in die Datei "karteiKartenSPCS.md".
2. Führe das Skript "parser.py" aus.
3. Gab es keine Fehler, wurde ein PDF mit A7-Karteikarten names "karteiKartenSPCS.pdf" erstellt

== SYNTAX DER FRAGEN UND ANTWORTEN ==

Titel: # Das ist ein Titel (bis zum Ende der Zeile)
Themengebiet: ## Das ist ein Themengebiet (bis zum Ende der Zeile)
Frage: ### Eine Frage mit Latex-Code $\phi$ (bis zum Ende der Zeile)
Antwort: Text unter Frage bis zur nächsten Frage oder bis Dateiende.


== BEISPIEL ==

# Mein cooles Fach

## Allgemeinwissen

### Wer wohnt in der Ananas ganz tief im Meer?
Spongebob Schwammkopf. $Formel mit \epsilon$


