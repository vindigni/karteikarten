#!/usr/bin/env python3

from enum import Enum, unique
import os


@unique
class State(Enum):
    NONE = 0
    VALID = 1
    INVALID = 2
    QUESTION = 3
    ANSWER = 4
    SECTION = 5
    SUBSECTION = 6


# INPUT_FILE = "sampleinput.md"
INPUT_FILE = "karteiKartenSPCS.md"
OUTPUT_FILE = "karteiKartenSPCS.tex"

QUESTION_START = "\\begin{karte}{"
QUESTION_END = "\n\end{karte}\n"
USEPACKAGE = "\\usepackage{"
USEPACKAGE_PARAMETER = "\\usepackage["
PARAMETER_PACKAGE_DIVIDER = "]{"
BEGIN_DOCUMENT = "\\begin{document}\n"
END_DOCUMENT = "\end{document}\n"
CONTENT = ''
CLOSING_BRACE = "}\n"
BEGIN_FIGURE = "\\begin{figure}\n\centering\n"
PICTURE_INCLUDE = "\includegraphics[width=0.8\pagewidth]{"
END_FIGURE = "\\end{figure}\n"
CURRENT_STATE = State.NONE
LAST_STATE = State.NONE

QUESTION_TAG = "### "
SECTION_TAG = "# "
SUBSECTION_TAG = "## "
PICTURE_TAG = "[img] "

SECTION = "\section*{"
SUBSECTION = "\subsection*{"

PACKAGES = [("inputenc", "utf8"), ("fontenc", "T1"),
            ("libertine", ""), ("babel", "ngerman"), ("graphicx", ""), ("amsmath", ""), ("braket", ""), ("amssymb", "")]

DOCUMENTCLASS = "\documentclass[a7paper,grid=both,print]{kartei}\n"

TEX_FILE = open(OUTPUT_FILE, "w+")


def init_tex():
    TEX_FILE.write(DOCUMENTCLASS)
    load_packages()
    TEX_FILE.write(BEGIN_DOCUMENT)


def close_tex():
    TEX_FILE.write(END_DOCUMENT)
    TEX_FILE.close()


def load_packages():
    for package in PACKAGES:
        TEX_FILE.write(build_package_statement(package[1], package[0]))


def build_package_statement(parameter, packagename):
    if parameter:
        return USEPACKAGE_PARAMETER + parameter + PARAMETER_PACKAGE_DIVIDER + packagename + CLOSING_BRACE
    else:
        return USEPACKAGE + packagename + CLOSING_BRACE


def process_file():
    with open(INPUT_FILE) as INPUT:
        print("Zeilen gelesen - parse Input")
        parse_markdown(INPUT.readlines())


def parse_markdown(text_input):
    question = False
    def end_previous_question():
        nonlocal question
        if question:
            TEX_FILE.write(QUESTION_END + "\n")
            question = False
            

    for string in text_input:
        #Create Questions
        if string.startswith(QUESTION_TAG):
            end_previous_question()

            TEX_FILE.write(QUESTION_START + string.strip(QUESTION_TAG + "\n") + CLOSING_BRACE)
            question = True
        elif string.startswith(SECTION_TAG):
            end_previous_question()
            TEX_FILE.write(
                SECTION + string.strip(SECTION_TAG).strip('\n') + CLOSING_BRACE)
        elif string.startswith(SUBSECTION_TAG):
            end_previous_question()
            TEX_FILE.write(
                SUBSECTION + string.strip(SUBSECTION_TAG).strip('\n') + CLOSING_BRACE)
        elif string.startswith(PICTURE_TAG):
            end_previous_question()
            TEX_FILE.write(
                BEGIN_FIGURE + PICTURE_INCLUDE + string.strip(PICTURE_TAG + '\n') + CLOSING_BRACE + END_FIGURE)
        elif not (string.isspace() or len(string) == 0):
            TEX_FILE.write(string)

    end_previous_question()



def set_current_state(prefix):
    if prefix is QUESTION_TAG:
        currentState = State.QUESTION
    elif prefix is ANSWER_TAG:
        currentState = State.ANSWER
    elif prefix is SECTION_TAG:
        currentState = State.SECTION
    elif prefix is SUBSECTION_TAG:
        currentState = State.SUBSECTION


def validate_input(linePrefix):
    lastState = CURRENT_STATE
    set_current_state(linePrefix)
    if CURRENT_STATE is State.QUESTION and (
                        lastState is State.SECTION or lastState is State.SUBSECTION or lastState is State.ANSWER):
        return True
    elif CURRENT_STATE is State.ANSWER and lastState is State.QUESTION:
        return True
    elif CURRENT_STATE is State.SECTION and (
                    lastState is State.SUBSECTION or lastState.ANSWER):
        return True



init_tex()
process_file()
close_tex()

# Run pdflatex to create pdf files
os.system("pdflatex -interaction nonstopmode -halt-on-error -file-line-error " + OUTPUT_FILE)
